﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KaladraShoot2 : MonoBehaviour
{
    public GameObject projectile;
    public Transform projectileSpawn;
    public float attackCooldown = 1.5f;

    public float power = 15;

    private float cd;
    
    void Update()
    {
        cd -= Time.deltaTime;
        
        if (Input.GetButtonDown("Fire2") && cd <= 0)
        {
            cd = attackCooldown;
            Shoot();
        }
    }

    void Shoot()
    {
        GameObject proj = Instantiate(projectile, projectileSpawn.position, Camera.main.transform.rotation);
        proj.transform.LookAt(Camera.main.transform.up * 100);
        proj.GetComponent<SeaShelter>().playerController = GetComponent<PlayerController>();
        proj.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * power, ForceMode.Impulse);
    }
}
