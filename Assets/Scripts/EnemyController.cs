﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float health = 10;
    public float maxHealth = 10;

    public Transform healthBar;
    
    public GameObject tentacleGrabPrefab;
    public GameObject tentacleGrabParent;

    public float giveHealthCd;
    public float pvLosePerSec = .5f;
    public PlayerController playerToGiveHealth;

    void Start()
    {
        
    }
    void Update()
    {
        if (giveHealthCd > 0)
        {
            giveHealthCd -= Time.deltaTime;
            float removedHealth = pvLosePerSec * Time.deltaTime;
            health -= removedHealth;
            playerToGiveHealth.GiveHealth(removedHealth);
            UpdateHealth();
        }
        else
        {
            playerToGiveHealth = null;
        }
    }

    public void UpdateHealth()
    {
        if (health < 0) health = 0;
        healthBar.transform.localScale = new Vector3(health / maxHealth, 1, 1);
    }

    public void TentacleGrab(PlayerController playerController)
    {
        giveHealthCd = 5;
        playerToGiveHealth = playerController;
        Destroy(Instantiate(tentacleGrabPrefab, tentacleGrabParent.transform), 5);
    }
}
