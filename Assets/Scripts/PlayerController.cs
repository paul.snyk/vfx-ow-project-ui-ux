﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float health = 10;
    public float maxHealth = 10;

    public Transform healthBar;
    
    public GameObject tentacleShelterGrabPrefab;
    public GameObject tentacleLeechingGrabPrefab;
    public Transform tentacleGrabParent;

    private GameObject tentacleShelter;
    private PlayerController playerShelter;
    private PlayerController playerLeeching;

    public float leechingArmCd;
    public float pvTookPerSec = .5f;

    public float inBioDomeCd;

    public ParticleSystem healParticle;

    

    void Start()
    {
        healParticle.Stop();
    }
    
    void Update()
    {
        if (leechingArmCd > 0)
        {
            leechingArmCd -= Time.deltaTime;
            float healthTook = pvTookPerSec * Time.deltaTime;
            
            healParticle.Play();
            
            GiveHealth(healthTook);
            playerLeeching.RemoveHealth(healthTook);
        }
        else
        {
            healParticle.Stop();
            playerLeeching = null;
        }

        if (playerShelter != null)
        {
            if (Vector3.Distance(playerShelter.transform.position, transform.position) > 8)
            {
                Destroy(tentacleShelter);
                playerShelter = null;
            }
        }

        if (Input.GetKeyDown(KeyCode.F) && tag == "allie")
        {
            DealDamage(2);
        }
        
        if (Input.GetKeyDown(KeyCode.T))
            RemoveHealth(2);

        inBioDomeCd -= Time.deltaTime;
    }

    public void UpdateHealth()
    {
        if (health < 0) health = 0;
        if (health > maxHealth) health = maxHealth;
        
        healthBar.transform.localScale = new Vector3(health / maxHealth, 1, 1);
    }

    public void SetBioDomeCd(float cd)
    {
        inBioDomeCd = cd;
    }

    public void GiveHealth(float healthGive)
    {
        health += healthGive;
        UpdateHealth();
    }

    public void RemoveHealth(float healthRemove)
    {
        if (tag == "allie" && playerShelter != null)
        {
            playerShelter.RemoveHealth(healthRemove * 0.2f);
            health -= healthRemove * 0.8f;
        }
        else
        {
            health -= healthRemove;
        }
        
        UpdateHealth();
    }

    public void FriendLeechingArm(PlayerController playerController)
    {
        if (playerLeeching == null)
        {
            leechingArmCd = 5;
            Destroy(Instantiate(tentacleLeechingGrabPrefab, tentacleGrabParent.transform), 5);
            playerLeeching = playerController;
        }
    }

    public void SeaShelter(PlayerController playerController)
    {
        if (playerShelter == null)
        {
            tentacleShelter = Instantiate(tentacleShelterGrabPrefab, tentacleGrabParent.transform);
            playerShelter = playerController;
        }
    }

    public void DealDamage(float damage)
    {
        if (tentacleShelter != null)
        {
            playerShelter.GiveHealth(damage * 0.6f);
        }

        if (inBioDomeCd > 0)
        {
            GiveHealth(damage * 0.25f);
        }
    }
}
