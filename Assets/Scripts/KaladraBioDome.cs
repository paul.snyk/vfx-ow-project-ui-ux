﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KaladraBioDome : MonoBehaviour
{
    public GameObject prefabBioDome;
    
    public float duration = 5;
    
    public float healthLosePerSec = .5f;

    private float cd;

    void Update()
    {
        cd -= Time.deltaTime;

        if (cd > 0)
        {
            GetComponent<PlayerController>().RemoveHealth(healthLosePerSec * Time.deltaTime);
        }

        if (Input.GetButtonDown("Fire3"))
        {
            UseBioDome();
        }
    }

    public void UseBioDome()
    {
        cd = duration;
        Destroy(Instantiate(prefabBioDome, transform.position, Quaternion.identity), duration);
    }
}
