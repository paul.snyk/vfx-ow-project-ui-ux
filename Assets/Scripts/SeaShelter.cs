﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaShelter : MonoBehaviour
{
    public GameObject tentacleParent;
    public PlayerController playerController;

    private void Update()
    {
        if (playerController == null)
        {
            playerController = GetComponentInParent<SeaShelter>().playerController;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        TentacleTrigger(other.gameObject);
    }

    public void TentacleTrigger(GameObject other)
    {
        if (other.tag == "allie")
        {
            other.GetComponent<PlayerController>().SeaShelter(playerController);
            Destroy(tentacleParent);
        }
    }
}
