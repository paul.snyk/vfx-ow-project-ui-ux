﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class magnetScript : MonoBehaviour
{
    private bool destroyed;
    private Animator animator;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        ExecuteAfterTime(4f);
    }
    void Update()
    {
        if (Vector3.Distance(GameObject.FindObjectOfType<FPSController>().transform.position, transform.position) >
            2.5f)
        {
            DestroyMagnet();
        }
    }

    public IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        DestroyMagnet();
    }

    public void DestroyMagnet()
    {
        if (!destroyed)
        {
            destroyed = true;
            animator.SetBool("destroyed", true);
            
            Destroy(gameObject, .5f);
        }
    }

    public void LookAtWall(Vector3 normal)
    {
        transform.LookAt(transform.position + normal);
    }
    
}
