﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnOtherScripts : MonoBehaviour
{
    public MyEvent eventToCall;
    
    [System.Serializable]
    public class MyEvent : UnityEvent<GameObject> {}

    void OnTriggerEnter(Collider other)
    {
        eventToCall.Invoke(other.gameObject);
    }
}
