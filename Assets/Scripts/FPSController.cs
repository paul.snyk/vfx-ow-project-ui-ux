﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;
using Random = UnityEngine.Random;

public class FPSController : MonoBehaviour
{
    public float gravity = 9.8f;
    private float vSpeed = 0;
    
    public float speed = 4;
    public float jumpForce = 5;
    
    public float mouseSensitivity = 3;

    public GameObject cameraObject;

    [Header("WallAndRoofRide")] 
    public int raycastSideNumber = 8;
    public LayerMask wallLayer;
    public int raycastTopNumber = 8;
    
    public GameObject tentacleMagnetPrefab;
    [Range(0f, 1f)]
    public float tentacleProb;

    private bool walkingInAir = false;
    
    private float rotX;
    
    private CharacterController characterController;
    private Rigidbody rigidbody;

    private Vector3 knockbackVector;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        CheckForWallAndRoof();
        
        // Get input dir
        Vector3 dir = Input.GetAxisRaw("Horizontal") * transform.right + Input.GetAxisRaw("Vertical") * transform.forward;
        dir = dir.normalized;

        // Rotate horizontal
        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * mouseSensitivity, 0));
        }
        
        // Rotate vertical
        if (Input.GetAxis("Mouse Y") != 0)
        {
            rotX -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            rotX = Mathf.Clamp(rotX, -90, 90);
            Quaternion rotation = Quaternion.Euler(rotX, 0, 0);
            
            cameraObject.transform.localRotation = rotation;
        }

        // if / else player is on the ground
        if (!characterController.isGrounded && !walkingInAir)
        {
            vSpeed -= gravity * Time.deltaTime;
        }
        else
        {
            // Jump
            if (Input.GetButtonDown("Jump"))
            {
                vSpeed = jumpForce;
            }
            

            if (walkingInAir && vSpeed > 0) vSpeed -= gravity * Time.deltaTime;
            else if (walkingInAir) vSpeed = 0;
        }

        // Decrease knockback vector if there is one
        if (knockbackVector != Vector3.zero)
        {
            if (knockbackVector.magnitude > .2)
                knockbackVector = Vector3.Lerp(knockbackVector, Vector3.zero, .05f);
            else
                knockbackVector = Vector3.zero;
        }
        
        // Apply movement
        characterController.Move((dir * speed + Vector3.up * vSpeed + knockbackVector) * Time.deltaTime);
    }

    public void CheckForWallAndRoof()
    {
        // Wall
        Vector3 nearestWallPoint = Vector3.zero;
        GameObject wallObject = null;
        Vector3 wallNormalVector = Vector3.up;

        
        for (int i = 0; i < raycastSideNumber; i++)
        {

            var angle = 360 / raycastSideNumber * i;
            
            var vForce = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.right;
            vForce = vForce.normalized;
            
            RaycastHit[] hits;
            hits = Physics.RaycastAll(transform.position, vForce * 2f, 2f, wallLayer);

            foreach (var hit in hits)
            {
                if (Vector3.Distance(transform.position, hit.point) <
                    Vector3.Distance(transform.position, nearestWallPoint) || nearestWallPoint != Vector3.zero)
                {
                    nearestWallPoint = hit.point;
                    wallObject = hit.collider.gameObject;
                    wallNormalVector = hit.normal;
                }
                
                if (Random.Range(0f, 1f) < tentacleProb*Time.deltaTime)
                {
                    CreateTentacleMagnet(hit.point, hit.normal);
                }
            }
        }

        if (wallObject != null)
        {
            if (Input.GetButton("Jump"))
            {
                walkingInAir = true;
            }
            else
            {
                walkingInAir = false;
            }
        }
        else
        {
            walkingInAir = false;
        }
        
        // Roof
        Vector3 nearestRoofPoint = Vector3.zero;
        GameObject roofObject = null;
        Vector3 roofNormalVector = Vector3.up;
        
        
        for (int i = 0; i < raycastSideNumber; i++)
        {

            var angle = 360 / raycastSideNumber * i;
            
            var vForce = (Quaternion.AngleAxis(angle, Vector3.up) * Vector3.right) * 0.5f + transform.up;
            vForce = vForce.normalized;
            
            RaycastHit[] hits;
            hits = Physics.RaycastAll(transform.position, vForce * 2f, 2f, wallLayer);

            foreach (var hit in hits)
            {
                if (Vector3.Distance(transform.position, hit.point) <
                    Vector3.Distance(transform.position, nearestRoofPoint) || nearestRoofPoint != Vector3.zero)
                {
                    nearestRoofPoint = hit.point;
                    roofObject = hit.collider.gameObject;
                    roofNormalVector = hit.normal;
                    
                    
                }

                if (Random.Range(0f, 1f) < tentacleProb*Time.deltaTime)
                {
                    CreateTentacleMagnet(hit.point, hit.normal);
                }
            }
        }

        if (!walkingInAir)
        {
            if (roofObject != null)
            {
                if (Input.GetButton("Jump"))
                {
                    walkingInAir = true;
                }
                else
                {
                    walkingInAir = false;
                }
            }
            else
            {
                walkingInAir = false;
            }
        }
    }

    private void CreateTentacleMagnet(Vector3 position, Vector3 normal)
    {
        GameObject magnet = Instantiate(tentacleMagnetPrefab, position, Quaternion.identity);
        magnet.GetComponent<magnetScript>().LookAtWall(normal);
    }

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < raycastSideNumber; i++)
        {
            var angle = 360 / raycastSideNumber * i;
            
            var vForce = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.right;
            
            Gizmos.DrawRay(transform.position, vForce*2f);
        }
        
        for (int i = 0; i < raycastTopNumber; i++)
        {
            var angle = 360 / raycastTopNumber * i;
            
            var vForce = (Quaternion.AngleAxis(angle, Vector3.up) * Vector3.right) * 0.5f + transform.up;
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, vForce*2f);
        }
    }
}
