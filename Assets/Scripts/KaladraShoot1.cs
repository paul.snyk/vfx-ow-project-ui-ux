﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KaladraShoot1 : MonoBehaviour
{
    public GameObject projectile;
    public Transform projectileSpawn;
    public float attackCooldown = 3;

    public int charge = 3;

    public float power = 15;

    private float cd;
    
    void Update()
    {
        cd -= Time.deltaTime;
        
        if (Input.GetButtonDown("Fire1") && cd <= 0 && charge > 0)
        {
            charge--;
            cd = attackCooldown;
            Shoot();
        }
            
        if (Input.GetKeyDown(KeyCode.A)) Recharge();
    }

    void Shoot()
    {
        GameObject proj = Instantiate(projectile, projectileSpawn.position,  Camera.main.transform.rotation);
        proj.transform.LookAt(Camera.main.transform.up * 100);
        proj.GetComponent<LeechingArm>().playerController = GetComponent<PlayerController>();
        proj.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * power, ForceMode.Impulse);
    }

    void Recharge()
    {
        charge = 3;
    }
}
