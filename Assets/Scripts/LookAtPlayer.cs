﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    private Transform playerTransform;
    
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("player").transform;
    }

    private void Update()
    {
        transform.LookAt(playerTransform);
    }
}
