﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BioDome : MonoBehaviour
{
    public float sphereRadius = 5;
    public float cd = 5;
    
    void Update()
    {
        cd -= Time.deltaTime;
        
        RaycastHit[] hits;
        hits = Physics.SphereCastAll(transform.position, sphereRadius, Vector3.up);

        foreach (var hit in hits)
        {
            if (hit.collider.tag == "allie")
            {
                hit.collider.GetComponent<PlayerController>().SetBioDomeCd(cd);
            }
        }
    }
}
