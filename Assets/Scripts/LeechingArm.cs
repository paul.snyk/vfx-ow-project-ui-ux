﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeechingArm : MonoBehaviour
{
    public GameObject tentacleParent;
    public PlayerController playerController;

    private void Update()
    {
        if (playerController == null)
        {
            playerController = GetComponentInParent<LeechingArm>().playerController;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        TentacleTrigger(other.gameObject);
    }

    public void TentacleTrigger(GameObject other)
    {
        if (other.tag == "ennemy")
        {
            other.GetComponent<EnemyController>().TentacleGrab(playerController);
            Destroy(tentacleParent);
        }

        if (other.tag == "allie")
        {
            other.GetComponent<PlayerController>().FriendLeechingArm(playerController);
            Destroy(tentacleParent);
        }

        if (other.tag == "Floor" || other.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }
}
