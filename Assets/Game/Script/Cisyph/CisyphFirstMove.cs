﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CisyphFirstMove : MonoBehaviour
{
    public List<GameObject> projectilList;
    private bool cooldown;
    
    public float timer;
    public int intTimer;
    public TMP_Text textCooldown;

    public Image imageKnifeReturn;
    
    
    void Start()
    {
        projectilList = new List<GameObject>();
        imageKnifeReturn.material.color = Color.white;
        
    }

    private void FixedUpdate()
    {
        
        if (!cooldown)
        {
            if (Input.GetButtonDown("ReturnKnife"))
            {
                Debug.Log("allo j'appuie sur a");
                for (int i = 0; i < projectilList.Count; i++)
                {
                    Debug.Log("je suis dans le count la");
                
                    projectilList[0].GetComponent<CisyphProjectil>().backTarget = true;
                    projectilList[i].GetComponent<CisyphProjectil>().backTarget = true;
                    projectilList.Remove(projectilList[i]);
                }
                timer = 10f;
            } 
        }

        if (timer > 0)
        {
            cooldown = true;
            timer -= Time.deltaTime;
            intTimer = Mathf.RoundToInt(timer);
            string textCd = intTimer.ToString();
            textCooldown.text = textCd;
            imageKnifeReturn.material.color = Color.grey;
        }

        if (timer <= 0)
        {
            cooldown = false;
            timer = 0;
            textCooldown.text = "";
            imageKnifeReturn.material.color = Color.white;
        }
        
    }

    public void AddEnemy(GameObject currentProjectil)
    {
        Debug.Log("add enemy list");
        if (!projectilList.Contains(currentProjectil))
        {
            projectilList.Add(currentProjectil);
        }
        ResetList();
    }

    public void ResetList()
    {
        for (int i = projectilList.Count - 1; i >= 0; i--)
        {
            if (projectilList[i] != null)
            {
                //On garde !
            }
            else
            {
                projectilList[i] = projectilList[projectilList.Count - 1];
                projectilList.RemoveAt(projectilList.Count - 1);
            }
        }
    }
}
