﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CisyphSecondMove : MonoBehaviour
{
    public GameObject prefab1;

    public GameObject prefab2;

    public Image imagecolor;
    public bool tttt;

    public Transform knifeSpawnPosition;

    public float powerknife;
    // Start is called before the first frame update
    void Start()
    {
        imagecolor.material.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("secondShoot"))
        {
            if (!tttt)
            {
                imagecolor.material.color = Color.yellow;
                GameObject projectil = Instantiate(prefab1, knifeSpawnPosition.position, Camera.main.transform.rotation);
                projectil.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * powerknife, ForceMode.Impulse);
                projectil.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0,360,0));
                tttt = true;
            }

            if (tttt)
            {
                GameObject projectil = Instantiate(prefab2, knifeSpawnPosition.position, Camera.main.transform.rotation);
                projectil.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * powerknife, ForceMode.Impulse);
                projectil.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0,360,0));
            }
            
        }

    }
}
