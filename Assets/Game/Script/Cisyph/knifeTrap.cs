﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class knifeTrap : MonoBehaviour
{
    public GameObject blueEffect;
    public GameObject purpleEffect;
    public GameObject knife;
    public GameObject enemyTouch;
    public GameObject player;

    public float speed = 1f;
    public float cooldownBeforeDestroy;
    

    public bool backTarget;
    public bool notReturn;
    private bool wallColision;
    public bool destroyCooldown;
    private bool resetAffichage = false;
    
    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            if (!wallColision)
            {
                wallColision = true;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                transform.position += direction / 300;
            }
            blueEffect.SetActive(false);
            Debug.Log("destruction knife");
            notReturn = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            
        }
        
    }
    
        
}
