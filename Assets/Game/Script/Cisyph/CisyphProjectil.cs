﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CisyphProjectil : MonoBehaviour
{
    public GameObject blueEffect;
    public GameObject purpleEffect;
    public GameObject knife;
    public GameObject enemyTouch;
    public GameObject player;

    public float speed = 1f;
    public float cooldownBeforeDestroy;
    

    public bool backTarget;
    public bool notReturn;
    private bool wallColision;
    public bool destroyCooldown;
    private bool resetAffichage = false;
    
    private Vector3 direction;



    // Start is called before the first frame update
    void Start()
    {
        backTarget = false;
        player = GameObject.FindWithTag("Player");
    }
    
    private void FixedUpdate()
    {
        if (!wallColision) direction = GetComponent<Rigidbody>().GetPointVelocity(transform.position);

        if (destroyCooldown)
        {
            cooldownBeforeDestroy += Time.deltaTime;
            if (!backTarget)
            {
                if (cooldownBeforeDestroy >= 6)
                {
                    Destroy(this.gameObject);
                    enemyTouch.GetComponent<AffichageKnifenumber>().knifeNumber -= 1;
                    destroyCooldown = false;
                }
            }
        }
        
        if (backTarget && !notReturn)
        {
            Debug.Log("c'est le retour mon frère");
            KnifeReturn();
        }
    }

    public void KnifeReturn()
    {
        //activation purple effect
        purpleEffect.SetActive(true);

        if (!resetAffichage)
        {
            enemyTouch.GetComponent<AffichageKnifenumber>().knifeNumber = 0;
            resetAffichage = true;
        }
        
        float speedBack = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speedBack);

        if (transform.position == player.transform.position)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            if (!wallColision)
            {
                wallColision = true;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                transform.position += direction / 300;
            }
            blueEffect.SetActive(false);
            Debug.Log("destruction knife");
            notReturn = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            Destroy(this.gameObject, 4f);
        }

        if (other.gameObject.CompareTag("ennemy"))
        {
            if (!wallColision)
            {
                wallColision = true;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                transform.position += direction / 400;
            }
            blueEffect.SetActive(false);
            Debug.Log("knife dans enemy");
            destroyCooldown = true;
            
            gameObject.GetComponent<BoxCollider>().enabled = false;

            other.gameObject.GetComponent<AffichageKnifenumber>().knifeNumber += 1;
            player.GetComponent<CisyphFirstMove>().AddEnemy(gameObject);
            enemyTouch = other.gameObject;
            enemyTouch.GetComponent<EnemyController>().health -= 1;
        }

        //si collision avec joueur pendant retour alors delete object
        if (other.gameObject.CompareTag("Player"))
        {
            if (backTarget)
            {
                Destroy(this.gameObject);
            }  
        }
    }
}
