﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CisyphPlayerShoot : MonoBehaviour
{
    [SerializeField] private float attackCooldownShoot;
    [SerializeField] private float powerKnife;
    
    public GameObject knifeProjectil;
    public Transform knifeProjectilSpawn;

    
    private float cooldown;
    void Update()
    {
        cooldown -= Time.deltaTime;

        if (Input.GetButtonDown("Fire1") && cooldown <= 0f)
        {
            cooldown = attackCooldownShoot;
            Shoot();
        }
    }

    private void Shoot()
    {
        GameObject projectil = Instantiate(knifeProjectil, knifeProjectilSpawn.position, Camera.main.transform.rotation);
        projectil.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * powerKnife, ForceMode.Impulse);
        projectil.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0,360,0));
    }
}
