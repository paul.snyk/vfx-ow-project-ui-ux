﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CisyphUltMove : MonoBehaviour
{
    [SerializeField]
    private float dashForce;

    public float timer;
    public float timerRoulade;
    public float timerCompress;
    public float timerReset;

    public bool timerActif;

    public int powerknife;

    public GameObject knife;

    public Image imageUlt;
    public TMP_Text textUlt;

    public GameObject[] directionKnife;
    public Transform knifeSpawnPosition;

    private void Start()
    {
        imageUlt.material.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (timerRoulade > 0)
        {
            timerRoulade -= Time.deltaTime;
            timerCompress = Mathf.Round(timerRoulade*10)/10;
            string textCd = timerCompress.ToString();
            textUlt.text = textCd;
        }
        
        if (timerReset > 0.01f)
        {
            timerReset -= Time.deltaTime;
            int timerResetInt = Mathf.RoundToInt(timerReset);
            string textCd = timerResetInt.ToString();
            textUlt.text = textCd;
        }
        
        if (!timerActif && timerReset >= 0.1f)
        {
            imageUlt.material.color = Color.grey;
        }
        else if (!timerActif && timerReset < 0)
        {
            imageUlt.material.color = Color.white;
        }
        
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }

        if (timerActif)
        {
            if (timer < 0)
            {
                timerReset = 10f;
                timerActif = false;
            }  
        }
        
        if (timerReset < 0.01f)
        {
            timerReset = 0;
        }
        
        if (timerReset <= 0)
        {
            if (Input.GetButtonDown("Ulti"))
            {
                timerActif = true;
                if (timer <= 0)
                {
                    timer = 10f;
                    UltiActivation();
                }
                else
                {
                    UltiActivation();
                }
            }
        }
    }

    void UltiActivation()
    {
        if (timerActif)
        {
            if (timerRoulade <= 0)
            {
                imageUlt.material.color = Color.yellow;
                GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * dashForce, ForceMode.Impulse);
                
                GameObject projectil = Instantiate(knife, knifeSpawnPosition.position, Camera.main.transform.rotation);
                projectil.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * powerknife, ForceMode.Impulse);
                projectil.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0,360,0));

                
                GameObject projectil2 = Instantiate(knife, directionKnife[0].transform.position,Camera.main.transform.rotation);
                projectil2.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * powerknife, ForceMode.Impulse);
                //projectil2.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0,360,0));
                
                
                GameObject projectil3 = Instantiate(knife, directionKnife[1].transform.position,Camera.main.transform.rotation);
                projectil3.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * powerknife, ForceMode.Impulse);
                //projectil3.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0,360,0));
                
                timerRoulade = 2.4f;
            }
        }
        
    }

}
