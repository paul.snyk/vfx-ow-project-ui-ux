﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AffichageKnifenumber : MonoBehaviour
{
    public float knifeNumber;

    public GameObject imageNumberKnife;

    public TMP_Text textNumberKnife;
    // Start is called before the first frame update
    void Start()
    {
        imageNumberKnife.SetActive(false);
        
    }
    
    private void FixedUpdate()
    {
        if (knifeNumber <= 0)
        {
            imageNumberKnife.SetActive(false);
        }

        if (knifeNumber >= 1)
        {
            imageNumberKnife.SetActive(true);
        }

        textNumberKnife.text = knifeNumber.ToString();
    }
}
