﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementcontroller : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float jumpRaycastDistance;

    [SerializeField] private float sensitivityMouse;
    public GameObject cameraGameobject;

    [SerializeField] private float rotationX;
    
    private Rigidbody _rb;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }

    // Update is called once per frame
    private void Update()
    {
        Jump();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        float horizontalAxis = Input.GetAxisRaw("Horizontal");
        float verticalAxis = Input.GetAxisRaw("Vertical");
        
        Vector3 movement = new Vector3(horizontalAxis, 0 , verticalAxis) * speed;

        Vector3 newPosition = _rb.position + _rb.transform.TransformDirection(movement);
        
        _rb.MovePosition(newPosition);

        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(new Vector3(0,Input.GetAxis("Mouse X") * sensitivityMouse, 0));
        }

        if (Input.GetAxis("Mouse Y") != 0)
        {
            rotationX -= Input.GetAxis("Mouse Y") * sensitivityMouse;
            rotationX = Mathf.Clamp(rotationX, -90, 90);
            Quaternion rotation = Quaternion.Euler(rotationX, 0,0);

            cameraGameobject.transform.localRotation = rotation;
        }
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (IsGrounded())
            {
                _rb.AddForce(0, jumpForce, 0, ForceMode.Impulse);
            }
        }
    }

    private bool IsGrounded()
    {
        return (Physics.Raycast(transform.position, Vector3.down, jumpRaycastDistance));
    }
}
